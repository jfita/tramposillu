/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.peritasoft.tramposillu.Tramposillu;

import java.util.Locale;

public class DesktopLauncher {
    public static void main(String[] args) {
        final String language = Locale.getDefault().getLanguage();
        final Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
        if (language.equals("ja")) {
            config.setTitle("トランポシーユ");
        } else if (language.equals("es")) {
            config.setTitle("Tramposillo");
        } else {
            config.setTitle("Tramposillu");
        }
        config.setWindowedMode(1280, 720);
        config.setWindowIcon(
                "icon128.png",
                "icon32.png",
                "icon16.png"
        );
        final Tramposillu game = new Tramposillu();
        new Lwjgl3Application(game, config);
    }
}
