/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.peritasoft.tramposillu.StageManager;
import com.peritasoft.tramposillu.Tramposillu;

public class StagesScreen extends TramposilluScreen {
    protected final StageManager stages;

    protected StagesScreen(final Tramposillu game, final StageManager stages) {
        super(game);
        if (stages == null) throw new IllegalArgumentException("stages can not be null");
        this.stages = stages;
    }

    @Override
    public void show() {
        super.show();
        Gdx.input.setInputProcessor(stages);
    }

    @Override
    public void resume() {
        super.resume();
        stages.resume();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        stages.resize();
    }

    @Override
    protected void act(float delta) {
        stages.act(delta);
    }

    @Override
    protected void draw(Batch batch) {
        stages.draw(batch);
    }
}
