/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.peritasoft.tramposillu.PeritaWinkAnimation;
import com.peritasoft.tramposillu.Tramposillu;

public class PeritaSoftScreen extends TramposilluScreen {
    private final PeritaWinkAnimation winkAnimation;

    public PeritaSoftScreen(Tramposillu game) {
        super(game);
        winkAnimation = new PeritaWinkAnimation(getAssets());
    }

    @Override
    public void show() {
        super.show();
        Gdx.input.setInputProcessor(new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                goNextScreen();
                return true;
            }

            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                goNextScreen();
                return true;
            }
        });
    }

    @Override
    protected void act(float delta) {
        if (winkAnimation.isFinished()) {
            goNextScreen();
        }
        super.act(delta);
        winkAnimation.act(delta);
    }

    @Override
    protected void draw(Batch batch) {
        super.draw(batch);
        winkAnimation.draw(batch, getWorldWidth(), getWorldHeight());
    }

    private void goNextScreen() {
        game.setScreen(new TitleScreen(game));
    }
}