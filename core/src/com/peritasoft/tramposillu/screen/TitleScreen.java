/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Scaling;
import com.peritasoft.tramposillu.Assets;
import com.peritasoft.tramposillu.TapAnimation;
import com.peritasoft.tramposillu.Tramposillu;
import com.peritasoft.tramposillu.Wobble;

import java.util.Locale;

public class TitleScreen extends TramposilluScreen {
    private static final float transitionTime = 1f;

    private final Letter[] letters;
    private final TextureRegion background;
    private final TapAnimation tapAnimation;
    private final Interpolation interpolation = new Interpolation.SwingOut(4f);
    private final Music music;
    private final int snoutLetter;
    private float stateTime;
    private boolean transition;
    private float scaleTime;

    public TitleScreen(final Tramposillu game) {
        super(game);

        music = getAssets().get(Assets.titleMusic);
        tapAnimation = new TapAnimation(getAssets());
        final TextureAtlas atlas = getAssets().get(Assets.spriteAtlas);
        background = atlas.findRegion("background");
        final Array<Sound> pop = new Array<>(new Sound[]{
                getAssets().get(Assets.popSound0),
                getAssets().get(Assets.popSound1),
                getAssets().get(Assets.popSound2)
        });
        final Array<Sound> squish = new Array<>(new Sound[]{
                getAssets().get(Assets.squishSound0),
                getAssets().get(Assets.squishSound1),
                getAssets().get(Assets.squishSound2)
        });

        final float yHigh = -60f;
        final float yLow = yHigh - 20f;
        final String language = Locale.getDefault().getLanguage();
        if (language.equals("ja")) {
            final Wobble poWobble = new Wobble(squish.random());
            letters = new Letter[]{
                    new Letter(atlas.findRegion("to"), -590f, yLow, 0.00f, pop.random(), new Wobble(squish.random())),
                    new Letter(atlas.findRegion("ra"), -430f, yLow, 0.05f, pop.random(), new Wobble(squish.random())),
                    new Letter(atlas.findRegion("n"), -265f, yLow, 0.10f, pop.random(), new Wobble(squish.random())),
                    new Letter(atlas.findRegion("ho"), -45f, yLow, 0.15f, pop.random(), poWobble),
                    new Letter(atlas.findRegion("maru"), +90f, yLow + 120f, 0.20f, pop.random(), poWobble),
                    new Letter(atlas.findRegion("shi"), +160f, yLow, 0.10f, pop.random(), new Wobble(squish.random())),
                    new Letter(atlas.findRegion("dash"), +345f, yHigh + 30f, 0.05f, pop.random(), new Wobble(squish.random())),
                    new Letter(atlas.findRegion("yu"), +465f, yLow, 0.00f, pop.random(), new Wobble(squish.random())),
            };
            snoutLetter = 4;
        } else {
            letters = new Letter[]{
                    new Letter(atlas.findRegion("t"), -610f, yHigh, 0.00f, pop.random(), new Wobble(squish.random())),
                    new Letter(atlas.findRegion("a"), -390f, yHigh, 0.10f, pop.random(), new Wobble(squish.random())),
                    new Letter(atlas.findRegion("p"), -100f, yHigh, 0.20f, pop.random(), new Wobble(squish.random())),
                    new Letter(atlas.findRegion("s"), +110f, yHigh, 0.30f, pop.random(), new Wobble(squish.random())),
                    new Letter(atlas.findRegion(language.equals("es") ? "o" : "u"), +470f, yLow, 0.00f, pop.random(), new Wobble(squish.random())),
                    new Letter(atlas.findRegion("r"), -490f, yLow, 0.05f, pop.random(), new Wobble(squish.random())),
                    new Letter(atlas.findRegion("m"), -250f, yLow, 0.15f, pop.random(), new Wobble(squish.random())),
                    new Letter(atlas.findRegion("o"), -10f, yLow, 0.30f, pop.random(), new Wobble(squish.random())),
                    new Letter(atlas.findRegion("i"), +230f, yLow, 0.15f, pop.random(), new Wobble(squish.random())),
                    new Letter(atlas.findRegion("big_l"), +320f, yLow, 0.10f, pop.random(), new Wobble(squish.random())),
                    new Letter(atlas.findRegion("small_l"), +410f, yLow + 40f, 0.05f, pop.random(), new Wobble(squish.random())),
            };
            snoutLetter = 7;
        }
    }

    @Override
    public void show() {
        super.show();
        Gdx.input.setInputProcessor(new InputAdapter() {
            @Override
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                getAssets().get(Assets.slideInSound).play();
                transition = true;
                return true;
            }
        });
        music.play();
    }

    @Override
    public void hide() {
        music.stop();
        super.hide();
    }

    @Override
    protected void act(float delta) {
        stateTime += delta;
        if (transition) {
            if (scaleTime > transitionTime) {
                game.setScreen(new ReadyScreen(game));
            }
            scaleTime += delta;
        }
    }

    @Override
    protected void draw(Batch batch) {
        final Vector2 size = Scaling.fill.apply(background.getRegionWidth(), background.getRegionHeight(),
                getWorldWidth(), getWorldHeight());
        batch.draw(background, 0, 0, size.x, size.y);

        final float centerX = getWorldWidth() / 2f;
        final float centerY = getWorldHeight() / 2f;
        for (final Letter letter : letters) {
            letter.draw(batch, stateTime, interpolation, centerX, centerY);
        }
        if (transition) {
            letters[snoutLetter].draw(batch, MathUtils.lerp(1f, 30f, Math.min(1f, scaleTime / transitionTime)), centerX, centerY);
        } else {
            tapAnimation.draw(batch, stateTime, centerX, centerY);
        }
    }

    private static class Letter {
        private final TextureRegion texture;
        private final float x;
        private final float y;
        private final float delay;
        private final Sound sound;
        private final Wobble wobble;
        private boolean played;

        private Letter(final TextureRegion texture, float x, float y, float delay, final Sound sound, final Wobble wobble) {
            this.texture = texture;
            this.x = x;
            this.y = y;
            this.delay = delay + 0.20f;
            this.sound = sound;
            this.wobble = wobble;
        }

        public void draw(final Batch batch, float stateTime, final Interpolation interpolation, float centerX, float centerY) {
            if (stateTime >= delay && !played) {
                sound.play();
                played = true;
            }
            final float scaleY = interpolation.apply(stateTime >= delay ? Math.min(1f, (stateTime - delay) / 0.5f) : 0f)
                    + wobble.apply(stateTime);
            final float scaleX = 1f - (scaleY - 1f);
            final float scaledWidth = texture.getRegionWidth() * scaleX;
            batch.draw(texture, centerX + x + (texture.getRegionWidth() - scaledWidth) / 2, centerY + y, scaledWidth, texture.getRegionHeight() * scaleY);
        }

        public void draw(final Batch batch, float scale, float centerX, float centerY) {
            final float scaledWidth = texture.getRegionWidth() * scale;
            final float scaledHeight = texture.getRegionHeight() * scale;
            batch.draw(texture, centerX + x + (texture.getRegionWidth() - scaledWidth) / 2, centerY + y + (texture.getRegionHeight() - scaledHeight) / 2, scaledWidth, scaledHeight);
        }
    }
}
