/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.screen;

import com.badlogic.gdx.audio.Music;
import com.peritasoft.tramposillu.Assets;
import com.peritasoft.tramposillu.StageManager;
import com.peritasoft.tramposillu.Tramposillu;

public class GoScreen extends StagesScreen {
    private final Music music;

    public GoScreen(final Tramposillu game, final StageManager stages) {
        super(game, stages);
        music = getAssets().get(Assets.goMusic);
    }

    @Override
    public void show() {
        super.show();
        stages.start();
        music.play();
    }

    @Override
    public void hide() {
        music.stop();
        super.hide();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (stages.isOver()) {
            game.setScreen(new EndScreen(game, stages));
        }
    }
}
