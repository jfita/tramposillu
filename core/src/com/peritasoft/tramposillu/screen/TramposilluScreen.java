/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.peritasoft.tramposillu.Assets;
import com.peritasoft.tramposillu.Tramposillu;

public class TramposilluScreen extends ScreenAdapter {
    protected final Tramposillu game;

    protected TramposilluScreen(final Tramposillu game) {
        if (game == null) throw new IllegalArgumentException("game can not be null");
        this.game = game;
    }

    protected Assets getAssets() {
        return game.getAssets();
    }

    protected float getWorldWidth() {
        return getViewport().getWorldWidth();
    }

    protected float getWorldHeight() {
        return getViewport().getWorldHeight();
    }

    protected Viewport getViewport() {
        return game.getViewport();
    }

    @Override
    public void render(float delta) {
        act(delta);

        Gdx.gl.glClearColor(250f / 255f, 250f / 255f, 210f / 255f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        final Batch batch = game.getBatch();
        batch.begin();
        draw(batch);
        batch.end();
    }

    protected void act(float delta) {
    }

    protected void draw(Batch batch) {
    }
}
