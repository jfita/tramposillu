/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;

public class Countdown {
    private final Animation<TextureRegion> animation;
    private float timer;

    public Countdown(final Assets assets) {
        final TextureAtlas atlas = assets.get(Assets.spriteAtlas);
        animation = new Animation<>(0.5f, atlas.findRegions("countdown"));
    }

    public void act(float delta) {
        timer += delta;
    }

    public void draw(Batch batch, float worldWidth, float worldHeight) {
        final TextureRegion frame = animation.getKeyFrame(timer, false);
        final float width = frame.getRegionWidth() * Math.abs(MathUtils.sin(MathUtils.lerp(0, MathUtils.PI, timer / animation.getFrameDuration())));
        batch.draw(frame,
                worldWidth / 2f - width / 2f, worldHeight / 2f - frame.getRegionHeight() / 2f,
                width, frame.getRegionHeight());
    }

    public boolean isFinished() {
        return animation.isAnimationFinished(timer);
    }
}
