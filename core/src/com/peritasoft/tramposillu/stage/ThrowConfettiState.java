/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.stage;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.tramposillu.Assets;
import com.peritasoft.tramposillu.ConfettiEmitter;

public class ThrowConfettiState extends State {
    private final TextureRegion pig;
    private final ConfettiEmitter confetti;
    private float x;
    private float offset;
    private float actionTime;

    public ThrowConfettiState(final Assets assets, final Stage stage, final TextureRegion pig, float actionTime, float offset) {
        super(assets, stage);
        if (pig == null) throw new IllegalArgumentException("pig cannot be null");
        this.pig = pig;
        this.actionTime = actionTime;
        this.offset = offset;

        confetti = new ConfettiEmitter(assets, stage.getWidth(), stage.getHeight());
    }

    @Override
    public void resize(float width, float height) {
        x = width / 2f;
    }

    @Override
    public State act(float deltaTime) {
        confetti.update(deltaTime);
        actionTime += deltaTime;
        offset = MathUtils.sin(actionTime / 0.5f) * 5f;
        return this;
    }

    @Override
    public void draw(Batch batch) {
        batch.draw(pig, x - pig.getRegionWidth() / 2f + offset, 50f);
        confetti.draw(batch);
    }
}
