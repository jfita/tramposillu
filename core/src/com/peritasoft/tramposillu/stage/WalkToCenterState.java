/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.stage;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.tramposillu.Assets;

public class WalkToCenterState extends State {
    private final Animation<TextureRegion> walkingAnimation;
    private float toX;
    private float actionTime;
    private float x;
    private boolean held;

    public WalkToCenterState(Assets assets, Stage stage) {
        super(assets, stage);

        final TextureAtlas atlas = assets.get(Assets.spriteAtlas);
        walkingAnimation = new Animation<>(0.099f, atlas.findRegions("walking"), Animation.PlayMode.LOOP);
    }

    @Override
    public void resize(float width, float height) {
        toX = width / 2f - Stage.PIG_WIDTH / 2f;
    }

    @Override
    public State act(float deltaTime) {
        final float duration = 3f;
        actionTime += deltaTime;
        if (actionTime > duration) {
            return held ? new BlowBubbleState(assets, stage) : new ChewGumState(assets, stage);
        }
        final float fromX = -50f;
        x = MathUtils.lerp(fromX, toX, Math.min(1f, actionTime / duration));
        return this;
    }

    @Override
    public void draw(Batch batch) {
        final TextureRegion frame = walkingAnimation.getKeyFrame(actionTime, true);
        batch.draw(frame, x, 50f);
    }

    @Override
    public State hold() {
        held = true;
        return super.hold();
    }

    @Override
    public State release() {
        held = false;
        return super.release();
    }
}