/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu.stage;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.peritasoft.tramposillu.Assets;
import com.peritasoft.tramposillu.HoldAnimation;

public class ChewGumState extends State {
    private final Animation<TextureRegion> chewingAnimation;
    private final HoldAnimation holdAnimation;
    private float x;
    private float actionTime;

    public ChewGumState(Assets assets, Stage stage) {
        super(assets, stage);

        holdAnimation = new HoldAnimation(assets);
        final TextureAtlas atlas = assets.get(Assets.spriteAtlas);
        chewingAnimation = new Animation<>(0.2f, atlas.findRegions("chewing"), Animation.PlayMode.LOOP);
    }

    @Override
    public void resize(float width, float height) {
        x = (width - Stage.PIG_WIDTH) / 2f;
    }

    @Override
    public State act(float deltaTime) {
        actionTime += deltaTime;
        return this;
    }

    @Override
    public void draw(Batch batch) {
        final TextureRegion frame = chewingAnimation.getKeyFrame(actionTime, true);
        batch.draw(frame, x, 50f);
        holdAnimation.draw(batch, actionTime, x, 100f);
    }

    @Override
    public State hold() {
        return new BlowBubbleState(assets, stage);
    }
}