/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Disposable;
import com.peritasoft.tramposillu.loader.MaybeMusicLoader;

public class Assets implements Disposable {
    public static final AssetDescriptor<TextureAtlas> spriteAtlas = new AssetDescriptor<>("sprites.atlas", TextureAtlas.class);
    public static final AssetDescriptor<Sound> winkSound = new AssetDescriptor<>("wink.mp3", Sound.class);
    public static final AssetDescriptor<Sound> cheerSound = new AssetDescriptor<>("cheer.mp3", Sound.class);
    public static final AssetDescriptor<Sound> sadSound = new AssetDescriptor<>("sad.mp3", Sound.class);
    public static final AssetDescriptor<Sound> squishSound0 = new AssetDescriptor<>("squish0.mp3", Sound.class);
    public static final AssetDescriptor<Sound> squishSound1 = new AssetDescriptor<>("squish1.mp3", Sound.class);
    public static final AssetDescriptor<Sound> squishSound2 = new AssetDescriptor<>("squish2.mp3", Sound.class);
    public static final AssetDescriptor<Sound> popSound0 = new AssetDescriptor<>("pop0.mp3", Sound.class);
    public static final AssetDescriptor<Sound> popSound1 = new AssetDescriptor<>("pop1.mp3", Sound.class);
    public static final AssetDescriptor<Sound> popSound2 = new AssetDescriptor<>("pop2.mp3", Sound.class);
    public static final AssetDescriptor<Sound> plopSound = new AssetDescriptor<>("plop.mp3", Sound.class);
    public static final AssetDescriptor<Sound> fallSound = new AssetDescriptor<>("fall.mp3", Sound.class);
    public static final AssetDescriptor<Sound> inflateSound = new AssetDescriptor<>("inflate.mp3", Sound.class);
    public static final AssetDescriptor<Sound> countdownSound = new AssetDescriptor<>("countdown.mp3", Sound.class);
    public static final AssetDescriptor<Sound> slideInSound = new AssetDescriptor<>("slidein.mp3", Sound.class);
    public static final AssetDescriptor<Music> titleMusic = new AssetDescriptor<>("title.mp3", Music.class);
    public static final AssetDescriptor<Music> readyMusic = new AssetDescriptor<>("ready.mp3", Music.class);
    public static final AssetDescriptor<Music> goMusic = new AssetDescriptor<>("go.mp3", Music.class);

    private final AssetManager manager = new AssetManager();

    public void load() {
        manager.setLoader(Music.class, new MaybeMusicLoader(manager.getFileHandleResolver()));
        manager.load(spriteAtlas);
        manager.load(winkSound);
        manager.load(cheerSound);
        manager.load(sadSound);
        manager.load(squishSound0);
        manager.load(squishSound1);
        manager.load(squishSound2);
        manager.load(popSound0);
        manager.load(popSound1);
        manager.load(popSound2);
        manager.load(plopSound);
        manager.load(fallSound);
        manager.load(inflateSound);
        manager.load(countdownSound);
        manager.load(slideInSound);
        manager.load(titleMusic);
        manager.load(readyMusic);
        manager.load(goMusic);
    }

    public <T> T get(AssetDescriptor<T> assetDescriptor) {
        return manager.get(assetDescriptor);
    }

    public void finishLoading() {
        manager.finishLoading();
    }

    @Override
    public void dispose() {
        manager.dispose();
    }
}
