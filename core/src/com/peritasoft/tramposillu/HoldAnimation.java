/*
    Tramposillu - a single-device multiplayer anything-goes cartoon-ish video game
    SPDX-FileCopyrightText: 2020-2021 David Pérez <dperez@peritasoft.com>
    SPDX-FileCopyrightText: 2020-2021 jordi fita i mas <jfita@peritasoft.com>

    SPDX-License-Identifier: GPL-3.0-or-later
 */
package com.peritasoft.tramposillu;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;

public class HoldAnimation {
    private final TextureRegion hand;
    private final TextureRegion hold;
    private float delay = 2f;


    public HoldAnimation(final Assets assets) {
        final TextureAtlas atlas = assets.get(Assets.spriteAtlas);
        hand = atlas.findRegion("hand");
        hold = atlas.findRegion("hold");
    }

    public void draw(Batch batch, float stateTime, float centerX, float centerY) {
        float time = stateTime - delay;
        if (time < 0f) return;
        if (time > 3f) {
            delay = stateTime + 5f;
            return;
        }
        final float x = centerX + hand.getRegionWidth() / 2f;
        if (time >= 1f && time <= 2f) {
            batch.draw(hold, x, centerY + 64f);
        }

        float scale = 1f;
        if (time >= 0.75f && time < 1f) {
            scale = MathUtils.lerp(1f, 0.90f, (time - 0.75f) / 0.25f);
        }
        if (time >= 1f && time < 2f) {
            scale = 0.90f;
        } else if (time >= 2f && time < 2.25f) {
            scale = MathUtils.lerp(0.90f, 1.0f, (time - 2f) / 0.25f);
        }
        batch.draw(hand, x, centerY, hand.getRegionWidth() * scale, hand.getRegionHeight() * scale);
    }
}
